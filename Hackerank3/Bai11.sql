package N21t4.Ex.hackerank;

public class Bai11 {
with table2 as
(select date1, count(date1), row_number() over(order by date1) rn from
(
    (select start_date as date1,count(start_date) as date_count from projects
    group by start_date)
    union all
    (select end_date as date1, count(end_date) as date_count from projects
    group by end_date)
    order by date1 asc) as table1
group by date1
having count(date1)=1)


select start_date, end_date from
(select date1 as start_date, row_number() over(order by date1) rn1 from table2 where mod(rn,2)=1) table3
join
(select date1  as end_date, row_number() over(order by date1) rn1 from table2 where mod(rn,2)=0) table4
on table3.rn1=table4.rn1
order by (end_date-start_date) asc, start_date asc
;
}
