package N21t4.Ex.hackerank;

public class Bai9 {
    select
    cl.hacker_id,
    hk.name,
    challange_count
    from (
            select
                    hacker_id,
            challange_count,
            count(1) over (partition by challange_count) as challange_count_rn,
    MAX(challange_count) over () as max_challange_count
    from (
            SELECT
                    hacker_id,
            count(1) as challange_count
    FROM Challenges
    group by hacker_id
    ) a
) cl
    LEFT JOIN Hackers hk ON cl.hacker_id = hk.hacker_id
            WHERE
    challange_count = max_challange_count
            or
    challange_count_rn = 1
    order by challange_count desc, hacker_id
}
