package N21t4.Ex.hackerank;

public class Bai8 {
select w.id, wp.age, w.coins_needed, w.power
from Wands w, Wands_Property wp
where w.code = wp.code
and w.coins_needed in (select min(wd.coins_needed) from Wands wd
                        where wd.code = wp.code
                        and wp.is_evil = 0
                                group by wd.power)
order by w.power desc, wp.age desc;
}
