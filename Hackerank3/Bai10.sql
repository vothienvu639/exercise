package N21t4.Ex.hackerank;

public class Bai10 {
SELECT hacker_id, name, sum(max_score) FROM (

    SELECT h.hacker_id, name, challenge_id, max(score) as max_score
    FROM Hackers h JOIN Submissions s ON h.hacker_id = s.hacker_id
    GROUP BY 1,2,3
    ) as cte

WHERE max_score != 0
GROUP BY 1,2
ORDER BY 3 DESC,1
}
