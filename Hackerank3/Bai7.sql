package N21t4.Ex.hackerank;

public class Bai7 {
SELECT concat( name, '(', left(occupation, 1), ')' )
FROM OCCUPATIONS
ORDER BY name;

SELECT concat( "There are a total of ", count(occupation), ' ', lower(occupation), 's.')
from OCCUPATIONS
group by occupation
order by count(occupation) ASC, occupation;
}
