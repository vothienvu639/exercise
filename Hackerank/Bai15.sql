package N19t4;

public class Bai15 {
with tab as ( select id,city,length(city) city_length,dense_rank() over (order by length(city) asc,city) city_rank from station), max_min as (select max(city_rank) max_rank,min(city_rank) min_rank from tab) select tab.city,tab.city_length from max_min,tab where (max_rank=city_rank or min_rank=city_rank);
}
