package N19t4.Hackerank2;

public class Bai43 {
SELECT comp.company_code, comp.founder, COUNT(DISTINCT lm.lead_manager_code) AS lead_managers, COUNT(DISTINCT sm.senior_manager_code) AS senior_managers, COUNT(DISTINCT mgr.manager_code) AS managers, COUNT(DISTINCT emp.employee_code) AS employees FROM company comp JOIN lead_manager lm ON lm.company_code = comp.company_code JOIN senior_manager sm ON sm.company_code = comp.company_code JOIN manager mgr ON mgr.company_code = comp.company_code JOIN employee emp ON emp.company_code = comp.company_code GROUP BY comp.company_code, comp.founder ORDER BY comp.company_code ASC
}
